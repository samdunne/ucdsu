<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

    <title>UCD Vote | Funding Policy</title>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


    <style type="text/css">
        body .thumbnails img {
            padding-left: 4%;
        }
    </style>
    <script type="text/javascript">

        var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29790126-1']);
            _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>

<body>
    <ul class="thumbnails">
        <img align="left" src="img/header.png" alt="Header Logo">
    </ul>
    <!-- Begin the nav bar -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="brand" href="#">UCDSU - Funding Policy Referendum</a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <li><a href="index"><i class="icon-home icon-white"></i> Home</a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-file icon-white"></i> The Funding Models<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?page=graduate">Graduate Tax</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?page=loans">Student Loan Scheme</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?page=fullfees">Full Up-front Fees</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?page=contribution">Student Contribution / <br /> Registration Fee</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?page=exchequer">Fully Exchequer funded</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-envelope icon-white"></i> Voting<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php?page=voting">About Voting</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?page=vote-how">How to Vote</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?page=returning">Returning Officer</a></li>
                                <li class="divider"></li>
                                <li><a href="index.php?page=regulations">Referendum Regulations</a></li>
                            </ul>
                        </li>
                        <li><a href="index.php?page=resources"><i class="icon-asterisk icon-white"></i> Key Resources</a></li>
                        <li><a href="index.php?page=about"><i class="icon-th-list icon-white"></i> About</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> 
