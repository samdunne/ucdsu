<h1>Mike Skinner</h1>

<p>The former frontman of The Streets, Mike Skinner has had numerous chart
successes in a career spanning well over a decade.</p>

<p>Following a career with The Streets, which included top 10 hits such as "Dry
Your Eyes", "Blinded By the Lights" and "Fit But You Know It", Skinner changed
his focus in 2011 to work on new material while keeping the same original and timeless style to his
productions.</p>

<p>When nominated for a coveted Mercury Award in 2002 for his work with The
Streets, Skinners response was "The critics favorite, the industries favorite and the bookies favortie; I think
this is a very remarkable record from a teenage young man talking about what
it's like to be young in the city.</p>
