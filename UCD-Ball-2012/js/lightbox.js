// display the lightbox
function lightbox(insertContent, ajaxContentUrl){

    // jQuery wrapper (optional, for compatibility only)
    (function($) {

        // add lightbox/shadow <div/>'s if not previously added
        if($('.box').size() == 0){
            var theLightbox = $('<div class="box"/>');
            var theShadow = $('<div class="backdrop"/>');
            $(theShadow).click(function(e){
                closeLightbox();
            });
            $('body').append(theShadow);
            $('body').append(theLightbox);
        }

        // remove any previously added content
        $('.box').empty();

        // insert HTML content
        if(insertContent != null){
            $('.box').append(insertContent);
        }

        // insert AJAX content
        if(ajaxContentUrl != null){
            // temporarily add a "Loading..." message in the lightbox
            $('.box').append('<p class="loading">Loading...</p>');

            // request AJAX content
            $.ajax({
                type: 'GET',
                url: ajaxContentUrl,
                success:function(data){
                    // remove "Loading..." message and append AJAX content
                    $('.box').empty();
                    $('.box').append(data);
                },
                error:function(){
                    alert('AJAX Failure!');
                }
            });
        }

        // move the lightbox to the current window top + 100px
        $('.box').css('top', $(window).scrollTop() + 100 + 'px');

        // display the lightbox
        $('.box').fadeIn(2000);
        $('.backdrop').fadeIn(1000);

        $(".backdrop").click(function() {
            closeLightbox();
        });

    })(jQuery); // end jQuery wrapper

}

// close the lightbox
function closeLightbox(){

    // jQuery wrapper (optional, for compatibility only)
    (function($) {

        // hide lightbox/shadow <div/>'s
        $('.box').fadeOut(1000);
        $('.backdrop').fadeOut(1000);

        // remove contents of lightbox in case a video or other content is actively playing
        $('.box').empty();

    })(jQuery); // end jQuery wrapper

}
