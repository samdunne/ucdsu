$(window).ready(function() {

    $curtainopen = false;
    $(".content").hide();
    $(".tickets").hide();
    $(".videos").hide();
    $("#buttons").hide();
    $(".cloud-ticket").hide();
    $(".cloud-date").hide();



    if ($.cookie('20120229') != '1') {
        lightbox(null, 'acts/welcome.txt');
        $.cookie('20120229', '1', { expires: 20 }); 

        $(".backdrop").click(function() {
            $(".leftcloud").stop().delay(3000).fadeOut(6000);;
            $(".rightcloud").stop().delay(3000).fadeOut(6000);;
            $("#logo").stop().delay(1000).animate({marginTop:'5px'}, 6000);
            $(".content").stop().delay(7000).fadeIn(3000);
            $(".tickets").stop().delay(7000).fadeIn(3000);
            $(".videos").stop().delay(7000).fadeIn(3000);
            $("#buttons").stop().delay(7000).fadeIn(3000);
        });
    } else {


        $(".leftcloud").stop().delay(3000).fadeOut(6000);
        $(".rightcloud").stop().delay(3000).fadeOut(6000);
        $(".cloud-ticket").stop().delay(7000).fadeIn(3000);
        $(".cloud-date").stop().delay(7000).fadeIn(3000);
        $("#logo").stop().delay(1000).animate({marginTop:'5px'}, 6000 );
        $(".content").stop().delay(7000).fadeIn(3000); 
        $(".tickets").stop().delay(7000).fadeIn(3000);
        $(".videos").stop().delay(7000).fadeIn(3000);
        $("#buttons").stop().delay(7000).fadeIn(3000);


    }



});

(function($) {
    $.cookie = function(key, value, options) {

        // key and at least value given, set cookie...
        if (arguments.length > 1 && (!/Object/.test(Object.prototype.toString.call(value)) || value === null || value === undefined)) {
            options = $.extend({}, options);

            if (value === null || value === undefined) {
                options.expires = -1;
            }

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            value = String(value);

            return (document.cookie = [
                encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
                ].join(''));
        }

        // key and possibly options given, get cookie...
        options = value || {};
        var decode = options.raw ? function(s) { return s; } : decodeURIComponent;

        var pairs = document.cookie.split('; ');
        for (var i = 0, pair; pair = pairs[i] && pairs[i].split('='); i++) {
            if (decode(pair[0]) === key) return decode(pair[1] || ''); // IE saves cookies with empty string as "c; ", e.g. without "=" as opposed to EOMB, thus pair[1] may be undefined
        }
        return null;
    };
})(jQuery);
